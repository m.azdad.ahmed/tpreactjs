import React, { Component } from 'react'
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import Team from './Team';
export default class ListeTeams extends Component {
    state={
        listeteams:[
        ],
        list:[
        ],
        page:1
    }
    componentWillMount(){
            const options = {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                Authorization: 'Bearer MblymQG9BRrwm6x6LOkJTM8MwyqS4nEwX9HWObXz-LITwtxPFzY'
                }
            };
            if(this.props.gameVideo=="all"){
            fetch('https://azdadma.vercel.app/api/teams?sort=&page=1&per_page=50', options)
                .then(response => response.json())
                .then(response => {
                    this.setState({
                        listeteams: response,
                        list:response.slice(0,5)
                        });
                }
                    
                    )
                .catch(err => console.error(err));
            }
            else{
                fetch('https://azdadma.vercel.app/api/'+this.props.gameVideo+'/teams?sort=&page=1&per_page=50', options)
                .then(response => response.json())
                .then(response => {
                    this.setState({
                        listeteams: response,
                        list:response.slice(0,5)
                        });
                }
                    
                    )
                .catch(err => console.error(err));
            }
            
    }
    
    handleChange = (event, value) => {

        this.setState({
            page:value
        });
            
    
            const options = {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                Authorization: 'Bearer MblymQG9BRrwm6x6LOkJTM8MwyqS4nEwX9HWObXz-LITwtxPFzY'
                }
            };
            
            fetch('https://azdadma.vercel.app/api/teams?sort=&page=1&per_page=50', options)
                .then(response => response.json())
                .then(response => {
                    this.setState({
                        listeteams: response,
                        list:response.slice((this.state.page-1)*5,this.state.page*5)
                        });
                })
                .catch(err => console.error(err));

    };
    render() {
        return (
            <div>
            <h1>
            Team
            </h1>
                {this.state.list.map((listeteam)=>(
                    <Team data={listeteam}/>
                    ))}
                    <div className="m">
                <Pagination count={this.state.listeteams.length/5}  onChange={this.handleChange} />
                </div>
            </div>
        )
    }
}
