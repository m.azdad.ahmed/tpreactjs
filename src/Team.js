import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { AccordionDetails, Button, CardActionArea, CardActions } from '@mui/material';
import { Link } from 'react-router-dom';
import { maxHeight } from '@mui/system';

const Team =(props) => {
    
    
    return (
        
        <Card sx={{ maxWidth: 500 ,maxHeight:200}} component="article" >
        <CardActionArea>
            <CardContent>
            <Typography gutterBottom variant="h5" component="div" >
            <img src={props.data.image_url} className="im"/>{props.data.name}
            </Typography>
            </CardContent>
        </CardActionArea>
        <CardActions>
            <Button size="small" color="primary" >
            <Link to={'/team/'+props.data.id } >Detail</Link>
            </Button>
        </CardActions>
        </Card>
    
    );
}
export default Team;