import React, { Component } from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Select, MenuItem} from '@mui/material';
import {Link} from 'react-router-dom'

export default class Appbaar extends Component {

  modifier = (event) => {
    this.props.setGameVideo(event.target.value)
    
  }

    render() {
        return (
          
            <div>
        <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            A-Sport Démo UBO
          </Typography>
          <Select style={{textDecoration:"none",color:"white"}} defaultValue='all games' onChange={this.modifier} >
                <MenuItem value={"all"} >All games</MenuItem>
                <MenuItem value={"csgo"}>CS-go</MenuItem>
                <MenuItem value={"codmw"}>Call of Duty</MenuItem>
                <MenuItem value={"dota2"}>Dota 2</MenuItem>
                <MenuItem value={"lol"}>League Of Legend</MenuItem>
                <MenuItem value={"pubg"}>PUBG</MenuItem>
                <MenuItem value={"ow"}>Overwatch</MenuItem>
                
          </Select>
          {this.props.a}
          <Button color="inherit"><Link to="/team" style={{textDecoration:"none",color:"white"}}>team</Link></Button>
          <Button color="inherit"><Link to="/leagues" style={{textDecoration:"none",color:"white"}} >leagues</Link></Button>
        </Toolbar>
      </AppBar>
    </Box>
            </div>

        )
    }
}
