import { Link } from 'react-router-dom'
import React from 'react'

export default function Pagenotefound() {
    return (
        <div>
            <h1>Page note found</h1>
            <Link to="/leagues">page d'accueil</Link>
        </div>
    )
}
