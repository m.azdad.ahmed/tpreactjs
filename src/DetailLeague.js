import React, { Component } from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { AccordionDetails, Button, CardActionArea, CardActions } from '@mui/material';
import { Link } from 'react-router-dom';
import { maxHeight } from '@mui/system';
import './detail.css'

export default class DetailLeague extends Component {
    
    state={
        leaguedetail:[
        ],
        videogame:{},
        series:[]
    }
    componentWillMount(){
        const options = {
            method: 'GET',
            headers: {
            Accept: 'application/json',
            Authorization: 'Bearer MblymQG9BRrwm6x6LOkJTM8MwyqS4nEwX9HWObXz-LITwtxPFzY'
            }
        };
        
        fetch(`https://azdadma.vercel.app/api/leagues/${this.props.match.params.leagueId}`, options)
            .then(response => response.json())
            .then(response => {
                this.setState({
                    leaguedetail: response,
                    videogame: response.videogame,
                    series: response.series,
                    });
            })
            .catch(err => console.error(err));
            
        }
    
    render() {
    
        return (
            <div>
            <Card sx={{ maxWidth: 900 ,maxHeight:2000}} component="section" >
            <CardActionArea>
                <CardContent>
                <Typography gutterBottom variant="h5" component="div" >
                <img src={this.state.leaguedetail.image_url} />
                </Typography>
                <Typography gutterBottom variant="h5" component="div" >
                <h1>{this.state.leaguedetail.name}</h1>
                </Typography>
                <Typography gutterBottom variant="h5" component="div" >
                {this.state.videogame.name}
                </Typography>
                </CardContent>
            </CardActionArea>
            </Card>
            
            {this.state.series.map(( serie, index ) => {
                    return (
                        <Card sx={{ maxWidth: 900 ,maxHeight:2000}} component="section" >
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div" >
                                    {serie.full_name}
                                </Typography>
                                <Typography gutterBottom variant="h7" component="div">
                                    From : {serie.begin_at} To : {serie.end_at}
                                </Typography>
                            </CardContent>
                            <Typography gutterBottom variant="h7" component="div">
                                    <Link to={'/teams/'+serie.winner_id} >{serie.winner_id ? "Vainqueur" : ""}</Link>
                                </Typography>
                             
                        </Card>
                    );
                })}

            </div>
        )
    }
}


