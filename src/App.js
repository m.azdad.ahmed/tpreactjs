import React, { Component, useState } from 'react'
import ListeLeagues from './ListeLeagues'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import DetailLeague from './DetailLeague';
import Pagenotefound from './Pagenotefound';
import Appbaar from './Appbaar'
import ListeTeams from './ListeTeams';
import DetailTeam from './DetailTeam';
import { Context } from './Context';
export default function App (){
  const [gameVideo,setGameVideo]=useState("all")
    return (
      <Context.Provider value={gameVideo}>
      <div>
        <Router>
          <Appbaar setGameVideo={setGameVideo}/>
          <Redirect from="/" to="leagues"/>
          <Switch>
            <Route path="/leagues" exact component={ListeLeagues}/>
            <Route path="/leagues/:leagueId" exact component={DetailLeague}/>
            <Route path="/team" exact component={ListeTeams}>
              <ListeTeams gameVideo={gameVideo}/>
            </Route>
            <Route path="/team/:teamId" exact component={DetailTeam}/>
            <Route component={Pagenotefound}/>
          </Switch>
        </Router>
      </div>
      </Context.Provider>
    )
  
}

