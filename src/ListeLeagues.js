import React, { Component } from 'react'
import League from './League'
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import './leagues.css'
export default class ListeLeagues extends Component {
    state={
        listleagues:[
        ],
        list:[
        ],
        page:1
    }
    componentWillMount(){
            const options = {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                Authorization: 'Bearer MblymQG9BRrwm6x6LOkJTM8MwyqS4nEwX9HWObXz-LITwtxPFzY'
                }
            };
            
            fetch('https://azdadma.vercel.app/api/leagues?sort=&page=1&per_page=50', options)
                .then(response => response.json())
                .then(response => {
                    this.setState({
                        listleagues: response,
                        list:response.slice(0,5)
                        });
                })
                .catch(err => console.error(err));
                
            }
            handleChange = (event, value) => {

                this.setState({
                    page:value
                });
                
                if(this.state.page>1){
                    
            
                const options = {
                    method: 'GET',
                    headers: {
                    Accept: 'application/json',
                    Authorization: 'Bearer MblymQG9BRrwm6x6LOkJTM8MwyqS4nEwX9HWObXz-LITwtxPFzY'
                    }
                };
            fetch('https://azdadma.vercel.app/api/leagues?sort=&page=1&per_page=50', options)
            .then(response => response.json())
            .then(response => {
                this.setState({
                    listleagues: response,
                    list:response.slice((this.state.page-1)*5,this.state.page*5)
                    });
            })
            .catch(err => console.error(err));
            }

            };
            
    render() {
        return (
            <div>
            <h1>
            Leagues
            </h1>
                {this.state.list.map((listleague)=>(
                    <League data={listleague}/>
                    ))}
                    <div className="m">
                <Pagination count={this.state.listleagues.length/5}  onChange={this.handleChange} />
                </div>
            </div>
        )
    }
}
