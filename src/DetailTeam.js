import React, { Component } from 'react'

export default class DetailTeam extends Component {
    state = { 
            team:[],
            players:[],
            videogame:{},
            video:{}
        }
         
         componentWillMount(){
                const options = {
                    method: 'GET',
                    headers: {
                    Accept: 'application/json',
                    Authorization: 'MblymQG9BRrwm6x6LOkJTM8MwyqS4nEwX9HWObXz-LITwtxPFzY'
                    }
                };
                
                fetch(`https://azdadma.vercel.app/api/teams/${this.props.match.params.teamId}`, options)
                    .then(response => response.json())
                    .then(response => {
                        this.setState({
                            team: response,
                            players: response.players,
                            videogame:response.current_videogame
                            });
                    })
                    .catch(err => console.error(err));
            }
         
         
    render() {
        let playersName = "";
        if(this.state.players.length !== 0){
            this.state.players.map((player,index) => (
                playersName += player.name + " "
            ))
        }
        return ( 
            <div  style={{textAlign: "center"}} >
                <img src={this.state.team.image_url} alt={this.state.team.name} width="300px" height="200px"/>
                <h1>{this.state.team.name}</h1>
                <p >Game : {this.state.videogame.name}</p>
                <span>{playersName}</span>
            </div>
        
        )
    }
}
